const URL = 'http://localhost:8080/api/posts';

const getPosts = () => fetch(URL)
  .then(response => {
    if (response.status === 200)
      return response.json();
  }).catch(error => new Error(error));

const createNote = (note) => fetch(
  URL,
  {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(note)
  }
)
  .then(response => {
    if (response.status === 201)
      return response.json();
    throw new Error('Failed to create a note!');
  }).catch(error => new Error(error));

const deleteNote = (id) => fetch(
  `${URL}/${id}`,
  {
    method: 'DELETE'
  }
).then(response => {
  if(response.status === 200)
    return response.json();
  throw new Error('Failed to delete a note!')
}).catch(error => new Error(error));

export {
  getPosts,
  createNote,
  deleteNote
}