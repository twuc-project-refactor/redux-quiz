import React, {Component} from 'react';
// import './App.less';
// vendors
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
// components
import Home from './components/home';
import Details from './components/details';
import Create from './components/create';
import Header from './components/header';

class App extends Component{
  render() {
    return (
      <>
        <Header />
        <Router>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/notes/:id(\d+)' component={Details}/>
            <Route path='/notes/create' component={Create}/>
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;