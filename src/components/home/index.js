import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from "react-router-dom";
// actions
import { getNotesAction } from "../../actions/noteActions";

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }
  
  componentDidMount() {
    this.props.getNotes();
  }

  handleClick() {
    this.props.history.push('/notes/create');
  }
  
  render() {
    const {id, titles} = this.props.notes;
    console.info(this.props.notes);
    
    return (
      <main>
        {
          id ?
            id.map(
              (id, index) => <Link key={id} to={`/notes/${id}`}>{titles[index]}</Link>
            ) : null
        }
        <button onClick={this.handleClick}>
          +
        </button>
      </main>
    )
  }
}

const mapStateToProps = state => ({
  notes: state.note.notes
});

const mapDispatchToProps = dispatch => ({
  getNotes: () => dispatch(getNotesAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
