import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render } from '@testing-library/react';
import Header from '../index';

test('should render Header component', () => {
  const { getByTestId } = render(<Header />);
  expect(getByTestId('header')).toHaveTextContent('This is Header');
});