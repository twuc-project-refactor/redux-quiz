import React from 'react';

export default function Header() {
  return (
    <h1 data-testid='header'>This is Header</h1>
  );
}