import React, { Component } from 'react';
import { connect } from 'react-redux';
// action
import { createNoteAction } from "../../actions/noteActions";

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: ''
    };
    
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  handleChange(event, key) {
    const value = event.target.value;
    this.setState({
      [key]: value
    });
  }

  handleClick(method) {
    if (method === 'cancle')
      this.props.history.push('/');
    if (method === 'submit') {
      this.props.createNote(
        this.state,
        this.props.notes,
        () => {
          this.props.history.push('/');
        });
    }
  }
  
  render() {
    const {title, description} = this.state;
    const isDisabled = !title || !description;
    return (
      <div>
        <section>
          <h4>标题</h4>
          <input
            type='text'
            value={title}
            onChange={event => this.handleChange(event, 'title')}
          />
        </section>
        <section>
          <h4>正文</h4>
          <textarea 
            value={description}
            onChange={event => this.handleChange(event, 'description')}
          />
        </section>
        <section>
          <button disabled={isDisabled} onClick={() => this.handleClick('submit')}>提交</button>
          <button onClick={() => this.handleClick('cancle')}>取消</button>
        </section>
      </div>
    )
  }
}

// const mapStateToProps = state => ({
//   notes: state.note.notes
// });

const mapDispatchToProps = dispatch => ({
  createNote: (note, notes, callback) => dispatch(createNoteAction(note, notes, callback))
});

// export default connect(mapStateToProps, mapDispatchToProps)(Create);
export default connect(() => ({}), mapDispatchToProps)(Create);