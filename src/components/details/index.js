import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import ReactMarkdown from 'react-markdown';

// actions
import { getNotesAction, deleteNoteAction } from "../../actions/noteActions";

class Details extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  
  
  componentDidMount() {
    this.props.getNotes();
  }

  handleClick(method) {
    if(method === 'return')
      this.props.history.push('/');
    if(method === 'delete') {
      const id = this.props.match.params.id;
      this.props.deleteNote(id, () => {this.props.history.push('/');});
    }
  }
  
  render() {
    const { id, titles, descriptions } = this.props.notes;
    const articleId = getIndexById(id, this.props.match.params.id);

    return (
      <div>
        This is No.{this.props.match.params.id} Details Page.
        <main>
          <aside>
            {
              id ?
                id.map(
                  (id, index) => 
                    <Link key={id} to={`/notes/${id}`}>{titles[index]}</Link>
                ) : null
            }
          </aside>
          <article>
            <h2>{titles[articleId]}</h2>
            <p>
              <ReactMarkdown source={descriptions[articleId]} />
            </p>
          </article>
          <section>
            <button onClick={() => this.handleClick('delete')}>删除</button>
            <button onClick={() => this.handleClick('return')}>返回</button>
          </section>
        </main>
      </div>
    )
  }
}

function getIndexById(ids, id) {
  return ids.indexOf(parseFloat(id));
}

const mapStateToProps = state => ({
  notes: state.note.notes
});

const mapDispatchToProps = dispatch => ({
  getNotes: () => dispatch(getNotesAction()),
  deleteNote: (id, callback) => dispatch(deleteNoteAction(id, callback))
});

export default connect(mapStateToProps, mapDispatchToProps)(Details);