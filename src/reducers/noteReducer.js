import { GET_NOTES, CREATE_NOTE, DELETE_NOTE } from '../actions/noteActions';

const initialState = {
  notes: {
    id: [],
    titles: [],
    descriptions: []
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_NOTES:
      return {
        ...state,
        notes: action.payload
      };

    case CREATE_NOTE:
      return {
        ...state
      };

    case DELETE_NOTE:
      return {
        ...state
      };

    default:
      return state;
  }
}