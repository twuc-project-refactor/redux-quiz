import {combineReducers} from "redux";
// reducers
import note from './noteReducer';

const reducers = combineReducers({
  note: note
});
export default reducers;