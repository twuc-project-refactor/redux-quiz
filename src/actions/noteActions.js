import { getPosts, createNote, deleteNote } from "../utils/fetchData";

const GET_NOTES = 'GET_NOTES';
const CREATE_NOTE = 'CREATE_NOTE';
const DELETE_NOTE = 'DELETE_NOTE';

const getNotesAction = () => dispatch => {
  getPosts()
    .then(data => {
      const id = getContentByKey(data, 'id');
      const titles = getContentByKey(data, 'title');
      const descriptions = getContentByKey(data, 'description');
      dispatch({
        type: GET_NOTES,
        payload: {
          id,
          titles,
          descriptions
        }
      });
    });
};

const createNoteAction = (note, notes, callback) => dispatch => {
  createNote(note)
    .then(() => {
        dispatch({type: CREATE_NOTE});
        callback();
    });
};

const deleteNoteAction = (id, callback) => dispatch => {
  deleteNote(id)
    .then(() => {
      dispatch({type: DELETE_NOTE});
      callback();
    });
};

function getContentByKey(data, key) {
  return data.map(item => item[key]);
}

function updateNotes(note, notes) {
  notes.titles.push(note.title);
  notes.descriptions.push(note.description);
  notes.id.push(notes.id[notes.length - 1] + 1);
  return notes;
}

export {
  GET_NOTES,
  CREATE_NOTE,
  DELETE_NOTE,
  getNotesAction,
  createNoteAction,
  deleteNoteAction
};