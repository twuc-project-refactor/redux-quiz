1. Fetchdata from backend ✅
2. Add getNotes reducer and action creator ✅
3. Show data in Home page ✅
4. Config react route rules ✅
5. Create detail page and show note detail ✅
6. Add create button in Home page ✅
7. Create create note component ✅
8. Add addNote reducer and action creator ✅
9. Implement back to Home page and refresh data after adding note successfully ✅
10. Add delete and return button in detail page ✅
11. Add deleteNote reducer and action creator, and return to Home page and refresh data after deleting note successfully (all actions are dispatched by note reducer) ✅
12. Add css